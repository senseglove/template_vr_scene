﻿//======= Copyright (c) Sense Glove, All rights reserved. ===============
//
// Purpose: Changing the SteamVR camerarig such that the user's currently
// faced direction becomes that of the chosen transform.
//
//=============================================================================

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SG;

/// <summary> Restores the user back to the base orientation, in case the tracking is off </summary>
/// <remarks> TODO: Store in PlayerPrefs in between sessions. </remarks>
public class FaceTargetTransform : MonoBehaviour
{
    //----------------------------------------------------------------------------------------------------------------------
    // Properties

    /// <summary> The camerRig object that controls the position of the VR setup within the scene, including that of the camera_eye. </summary>
    [Header("Required Components")]
    [Tooltip("The camerRig object that controls the position of the VR setup within the scene, including that of the camera_eye.")]
    public Transform cameraRig;

    /// <summary> The HMD within the cameraRig. </summary>
    [Tooltip("The HMD within the cameraRig.")]
    public Transform camera_eye;

    /// <summary> The desired transform for the camera to take </summary>
    [Tooltip("The desired transform for the camera to take")]
    [SerializeField]
    protected Transform targetTransform;



    /// <summary> If set to true, the camera must match the target position. </summary>
    [Header("Settings")]
    [Tooltip("If set to true, the camera must match the target position.")]
    [SerializeField]
    protected bool matchPosition = true;

    /// <summary> If set to true, the camera must match the target rotation. </summary>
    [Tooltip("If set to true, the camera must match the target rotation.")]
    [SerializeField]
    protected bool matchRotation = true;

    /// <summary> Whether to ignore the Y component of the camerarig when facing the correct position </summary>
    [Tooltip("Whether to ignore the Y component of the camerarig when facing the correct position.")]
    [SerializeField]
    protected bool ignoreY = false;

    /// <summary> Set to true to load the rig rotation from last session. </summary>
    [SerializeField]
    [Tooltip("Set to true to load the rig rotation from last session.")]
    protected bool loadLastRotation = false;
    

    [Header("Function Calls")]
    /// <summary> Optional Key to run this script manually. </summary>
    [Tooltip("Optional Key to run this script manually.")]
    public KeyCode faceTargetKey = KeyCode.None;


    /// <summary> Used to load playerprefs AFTER all other scripts have had a change to run their Start() function. </summary>
    protected bool init = false;

    /// <summary> PlayerPrefs registry key for the last camera (y) rotation. </summary>
    protected static readonly string rKey = "rigRotation";
    protected static readonly string rigXKey = "rigX", rigYKey = "rigY", rigZKey = "rigZ";


    //----------------------------------------------------------------------------------------------------------------------
    // Functions

    /// <summary> Tell this script's camera to face the chosen transform with the script's current parameters. </summary>
    /// <param name="newTransform"></param>
    public void FaceTarget(Transform newTransform)
    {
        this.FaceTarget(newTransform, this.matchPosition, this.matchRotation);
    }

    /// <summary> Tell this script's camera to face the chosen transform with optional parameters. </summary>
    /// <param name="newTransform"></param>
    /// <param name="setPosition"></param>
    /// <param name="setRotation"></param>
    public void FaceTarget(Transform newTransform, bool setPosition, bool setRotation)
    {
        if (this.cameraRig != null)
        {
            Debug.Log("Facing Table");

            if (this.camera_eye != null)
            {
                //Set Orientation
                if (setRotation)
                {
                    Quaternion oldRotation = this.cameraRig.rotation;

                    float yTarget = SG_Util.NormalizeAngle(newTransform.rotation.eulerAngles.y);
                    float yRig = SG_Util.NormalizeAngle(this.cameraRig.rotation.eulerAngles.y);
                    float yCam = SG_Util.NormalizeAngle(this.camera_eye.rotation.eulerAngles.y);

                    //Debug.Log("yTarget = " + yTarget + ", yRig = " + yRig + ", yCam = " + yCam);

                    float newYrot = yTarget + yRig - yCam; //Actually it's yRig - (yRig - yTarget) + (yRig - yCam), but simplified.
                    Quaternion newRotation = Quaternion.Euler(0, newYrot, 0);
                    this.cameraRig.rotation = newRotation;

                    PlayerPrefs.SetFloat(FaceTargetTransform.rKey, newYrot);
                    this.OnRigMoved(oldRotation, newRotation);
                }

                //Set Position
                if (setPosition)
                {
                    Vector3 dPos = this.camera_eye.position - this.cameraRig.position;

                    float newY = ignoreY ? this.cameraRig.position.y : newTransform.position.y;

                    Vector3 newpos = new Vector3
                        (
                            newTransform.position.x - dPos.x,
                            newY,
                            newTransform.position.z - dPos.z
                        );
                    this.cameraRig.position = newpos;

                    PlayerPrefs.SetFloat(rigXKey, this.cameraRig.position.x);
                    PlayerPrefs.SetFloat(rigYKey, this.cameraRig.position.y);
                    PlayerPrefs.SetFloat(rigZKey, this.cameraRig.position.z);

                    //Debug.Log("Set position to " + SenseGlove_Util.ToString(this.cameraRig.position));
                }
            }
            else
            {
                this.cameraRig.position = newTransform.position;
            }
        }
        else
        {
            Debug.Log(this.name + ".FaceTargetTransform requires access to a CameraRig.");
        }
    }

    /// <summary> load the last rotation from Playerprefs, if any exisits. </summary>
    protected void ApplyLastRotation()
    {
        if (this.cameraRig != null)
        {
            //apply rotation
            if (PlayerPrefs.HasKey(FaceTargetTransform.rKey) && this.matchRotation)
            {
                Quaternion currRot = this.cameraRig.rotation;
                Quaternion desiredRot = Quaternion.Euler(0, PlayerPrefs.GetFloat(FaceTargetTransform.rKey, 0), 0);

                this.cameraRig.transform.rotation = desiredRot;

                this.OnRigMoved(currRot, desiredRot);
            }

            //apply position
            if (this.matchPosition)
            {
                Vector3 prevPos = Vector3.zero;
                prevPos.x = PlayerPrefs.GetFloat(rigXKey, 0);
                prevPos.y = PlayerPrefs.GetFloat(rigYKey, 0);
                prevPos.z = PlayerPrefs.GetFloat(rigZKey, 0);
                this.cameraRig.position = prevPos;
                //Debug.Log("Loaded position as " + SenseGlove_Util.ToString(this.cameraRig.position));
                //Transform newTransform = this.targetTransform;

                //if (this.camera_eye != null)
                //{
                //    Vector3 dPos = this.camera_eye.position - this.cameraRig.position;
                //    float newY = ignoreY ? this.cameraRig.position.y : newTransform.position.y;
                //    Vector3 newpos = new Vector3
                //        (
                //            newTransform.position.x - dPos.x,
                //            newY,
                //            newTransform.position.z - dPos.z
                //        );
                //    this.cameraRig.position = newpos;
                //}
                //else
                //    this.cameraRig.position = newTransform.position;

            }
        }
    }

    //----------------------------------------------------------------------------------------------------------------------
    // Monobehaviour

    // Update is called once per frame
    void Update ()
    {
        if (!this.init)
        {
            if (this.loadLastRotation)
                this.ApplyLastRotation();
            this.init = true;
        }

        if (Input.GetKeyDown(this.faceTargetKey))
        {
            this.FaceTarget(this.targetTransform);
        }

	}


    public delegate void RigMovedHandler(object source, Rijndam.RigEvent args);

    public event RigMovedHandler RigMoved;

    protected void OnRigMoved(Quaternion oldR, Quaternion newR)
    {
        float newY = newR.eulerAngles.y;
        PlayerPrefs.SetFloat(FaceTargetTransform.rKey, newY);

        if (RigMoved != null)
        {
            RigMoved(this, new Rijndam.RigEvent(oldR, newR));
        }
    }



}


namespace Rijndam
{

    public class RigEvent : System.EventArgs
    {
        public Quaternion oldRotation;
        public Quaternion newRotation;
        public Quaternion dRig;

        public RigEvent(Quaternion oldR, Quaternion newR)
        {
            this.oldRotation = oldR;
            this.newRotation = newR;
            this.dRig = Quaternion.Inverse(newR) * oldR;
        }
    }

}

