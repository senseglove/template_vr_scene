﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SG_RememberTrackedObj : MonoBehaviour
{

    private static string leftKey = "lastTrackL", rightKey = "lastTrackR";
    public SG.SG_TrackedHand leftHand, rightHand;
    public string leftName = "", rightName = "";
    public KeyCode switchObjectsKey = KeyCode.None;
    public bool runAtStart = false;

    public void StoreCurrentTracks()
    {
        string leftObj = leftHand.trackedObject != null ? leftHand.trackedObject.name : "";
        string rightObj = rightHand.trackedObject != null ? rightHand.trackedObject.name : "";

        PlayerPrefs.SetString(leftKey, leftObj);
        PlayerPrefs.SetString(rightKey, rightObj);

        leftName = leftObj;
        rightName = rightObj;

        //Debug.LogError("Stored last tracked objects: L="+leftObj + ", R=" + rightObj);
    }


    protected void ApplyTackedObject(SG.SG_TrackedHand hand, string name)
    {
        if (name.Length > 0)
        {
            GameObject obj = GameObject.Find(name);
            if (obj != null)
            {
                hand.trackedObject = obj.transform;
            }
        }
    }

    public void ApplyLastTracks()
    {
        string leftObj = PlayerPrefs.GetString(leftKey, "");
        string rightObj = PlayerPrefs.GetString(rightKey, "");
        //Debug.LogError("Loaded last tracked objects: L=" + leftObj + ", R=" + rightObj);

        ApplyTackedObject(leftHand, leftObj);
        ApplyTackedObject(rightHand, rightObj);

        //Debug.LogError("Applied: L=" + leftHand.trackedObject.name + ", R=" + rightHand.trackedObject.name);


        leftName = leftHand.trackedObject != null ? leftHand.trackedObject.name : "";
        rightName = rightHand.trackedObject != null ? rightHand.trackedObject.name : "";
    }

    public void SwitchObjects()
    {
        GameObject leftObj = leftHand.trackedObject.gameObject;
        leftHand.trackedObject = rightHand.trackedObject;
        rightHand.trackedObject = leftObj.transform;
        StoreCurrentTracks();
    }


    // Start is called before the first frame update
    void Start()
    {
        if (runAtStart) { ApplyLastTracks(); }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            this.StoreCurrentTracks();
        }
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            this.ApplyLastTracks();
        }
        if (Input.GetKeyDown(this.switchObjectsKey))
        {
            this.SwitchObjects();
        }
    }

    private void OnApplicationQuit()
    {
        StoreCurrentTracks();
    }

}
