﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SG;

public class UIMenuScript : MonoBehaviour
{
    [Header("Menu")]
    public RectTransform rectTransformMenu;
    private bool inOrOut = false;
    private float slideMaxDistance = 0;
    private float slidedDistance = 0;

    [Header("Version Number")]
    public Text appVersionNumber;

    [Header("Height Slider")]
    public Slider heightSlider;
    public GameObject vrCameraRig;
    private float cameraRigStartPos;

    [Header("Trackers")]
    public GameObject tracker1;
    public GameObject tracker2;
    public Toggle trackerToggle;
    private bool showTrackers = false;

    [Header("Feedback toggles")]
    public SG_SenseGloveHardware senseGloveR;
    public SG_SenseGloveHardware senseGloveL;
    public Toggle vibrotactileToggle;
    private bool vibrotactileFeedback = true;
    public Toggle forceToggle;
    private bool forceFeedback = true;

    private void Start()
    {
        // place the menu outside the screen as start
        rectTransformMenu.transform.Translate(rectTransformMenu.rect.width, 0f, 0f);
        slideMaxDistance = rectTransformMenu.rect.width + 50;

        // set the Application version number from the playersettings
        appVersionNumber.text = "App Version: " + Application.version;

        // hide the trackers on start
        SetTrackers();

        // set the start position of the camera rig
        cameraRigStartPos = vrCameraRig.transform.position.y;
    }

    private void Update()
    {
        // Slide the menu in and out of the screen
        if (inOrOut && slidedDistance < slideMaxDistance)
        {
            rectTransformMenu.transform.Translate(-1000 * Time.deltaTime, 0, 0);
            slidedDistance = slidedDistance + (1000 * Time.deltaTime);
        }
        else if (!inOrOut && slidedDistance > 0)
        {
            rectTransformMenu.transform.Translate(1000 * Time.deltaTime, 0, 0);
            slidedDistance = slidedDistance - (1000 * Time.deltaTime);
        }

        // set the height of the camera
        vrCameraRig.transform.position = new Vector3(vrCameraRig.transform.position.x, cameraRigStartPos + heightSlider.value, vrCameraRig.transform.position.z);
    }

    private void SetTrackers()
    {
        tracker1.SetActive(showTrackers ? true : false);
        tracker2.SetActive(showTrackers ? true : false);
        trackerToggle.isOn = showTrackers;
}

    public void ResetScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ExitProgram()
    {
        Application.Quit();
    }

    // Buttons
    public void ButtonMenu()
    {
        inOrOut = !inOrOut;
    }

    public void ToggleShowTrackers()
    {
        showTrackers = !showTrackers;
        SetTrackers();
    }

    public void ToggleVibrotactileFeedback()
    {
        vibrotactileFeedback = !vibrotactileFeedback;
        senseGloveR.buzz_Enabled = vibrotactileFeedback ? true : false;
        senseGloveL.buzz_Enabled = vibrotactileFeedback ? true : false;
        vibrotactileToggle.isOn = vibrotactileFeedback;
    }

    public void ToggleForceFeedback()
    {
        forceFeedback = !forceFeedback;
        senseGloveR.FFB_Enabled = forceFeedback ? true : false;
        senseGloveL.FFB_Enabled = forceFeedback ? true : false;
        forceToggle.isOn = forceFeedback;
    }
}
